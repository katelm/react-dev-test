import React, { Component } from "react";

class ShowResultCard extends Component {

	createCards(show){
		return <li key={show.id} className="resultCard"> 
					<h4>{show.name}</h4>
					<img src={show.image} alt={show.name}/>
            	</li>
	}

	render() { 
		const shows = this.props.shows;
		if (shows.length > 0) {
      		let showCards = shows.map(this.createCards);
			return <ul>{showCards}</ul>;
    	} else {
			return <div className="noResults">No matching shows found. Please try again.</div>;
    	}
	}
}

export default ShowResultCard;