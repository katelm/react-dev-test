import React from 'react';
import './App.css';
import ShowSearch from './ShowSearch.js';

function App() {
  return (
    <div className="App">
      <ShowSearch className="searchForm"/>
    </div>
  );
}

export default App;
