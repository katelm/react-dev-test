import React, { Component } from "react";
import ShowResultCard from './ShowResultCard.js';
 
class ShowSearch extends Component {

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      shows: []
    };
    this.getTVShowNames = this.getTVShowNames.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount(){
    this.getTVShowNames("superman");
  }

  submitSearch(e){
    const query = e.target.elements.query.value;
    e.preventDefault();
    e.target.elements.query.value = "";
    this.getTVShowNames(query);
  }

  getTVShowNames(query) {
    fetch("http://api.tvmaze.com/search/shows?q=" + query)
      .then(res => res.json())
      .then(
        (result) => {
          this.handleResults(result);
          this.setState({
            isLoaded: true,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  handleResults(result){
    let sortedShows = [];
    for (let j = 0; j < result.length; j++) {
      let show = result[j].show;
      let img = show.image != null ? show.image.medium : "https://via.placeholder.com/210x295.png?text=No+Image+Available";
      sortedShows.push({id: show.id, name: show.name, image: img});
    }
    sortedShows.sort((a, b) => (a.name > b.name) ? 1 : -1);
    console.log(sortedShows);
    console.log(sortedShows.map(show => show.name));

    this.setState({
      shows: sortedShows
    });
  }

  render() {
    return (
      <section>
        <form onSubmit={this.submitSearch} className="searchForm">
          <div>
            Enter a word to search for TV shows:<br/>
            <input type="text" name="query" placeholder="ex: 'superman'" className="searchInput"/>
            <button type="submit">Search</button>
          </div>
          <br />
        </form>
        <ShowResultCard shows={this.state.shows}/>
      </section>
    );
  }

}
 
export default ShowSearch;